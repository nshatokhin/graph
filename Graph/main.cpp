#include "Graph.h"
#include "Helpers.h"

#include <iostream>

int main()
{
	Graph graph(10000);

	Graph_CreateGrid(graph, 50, 50, 10, 10);

	std::cout << graph.GetEdge(2, 1).cost << std::endl;

	for (IdxType i = 0; i < graph.numNodes(); i++)
	{
		//std::cout << graph.GetNode(i).p << std::endl;
	}

    return 0;
}

