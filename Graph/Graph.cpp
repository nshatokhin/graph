#include "Graph.h"

#include <algorithm>
#include <cassert>

Graph::Graph(IdxType maxNodes, bool isDigraph) :
	m_digraph(isDigraph),
	m_maxNodesCount(maxNodes),
	m_nodesCount(0),
	m_nodes(maxNodes), m_edges(maxNodes)
{
}

Graph::~Graph()
{
}

const Graph::NodeType & Graph::GetNode(IdxType idx) const
{
	assert((idx < m_nodesCount) && (idx >= 0));

	return m_nodes[idx];
}

Graph::NodeType & Graph::GetNode(IdxType idx)
{
	assert((idx < m_nodesCount) && (idx >= 0));

	return m_nodes[idx];
}

const Graph::EdgeType & Graph::GetEdge(IdxType from, IdxType to) const
{
	assert((from < m_nodesCount) && (from >= 0) && m_nodes[from].index != invalid_node_index);

	assert((to < m_nodesCount) && (to >= 0) && m_nodes[to].index != invalid_node_index);

	for (EdgeList::const_iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to) return *curEdge;
	}

	assert(false);
}

Graph::EdgeType & Graph::GetEdge(IdxType from, IdxType to)
{
	assert((from < m_nodesCount) && (from >= 0) && m_nodes[from].index != invalid_node_index);

	assert((to < m_nodes.size()) && (to >= 0) && m_nodes[to].index != invalid_node_index);

	for (EdgeList::iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to) return *curEdge;
	}

	assert(false);
}

IdxType Graph::addNode(const Node &node)
{
	assert(m_nodesCount < m_maxNodesCount);

	if (node.index < m_nodesCount)
	{
		//make sure the client is not trying to add a node with the same ID as
		//a currently active node
		assert(m_nodes[static_cast<NodeIndex>(node.index)].index == invalid_node_index);

		m_nodes[static_cast<NodeIndex>(node.index)] = node;

		return m_nodesCount;
	}
	else
	{
		//make sure the new node has been indexed correctly
		assert(node.index == m_nodesCount);

		m_nodes[m_nodesCount] = node;
		m_edges[m_nodesCount] = EdgeList();

		return m_nodesCount++;
	}
}

void Graph::addEdge(const Edge &edge)
{
	//first make sure the from and to nodes exist within the graph 
	assert((edge.from < m_nodesCount) && (edge.to < m_nodesCount));

	//make sure both nodes are active before adding the edge
	if ((m_nodes[edge.to].index != invalid_node_index) &&
		(m_nodes[edge.from].index != invalid_node_index))
	{
		//add the edge, first making sure it is unique
		if (isUniqueEdge(edge.from, edge.to))
		{
			m_edges[edge.from].push_back(edge);
		}

		//if the graph is undirected we must add another connection in the opposite
		//direction
		if (!m_digraph)
		{
			//check to make sure the edge is unique before adding
			if (isUniqueEdge(edge.to, edge.from))
			{
				EdgeType NewEdge = edge;

				NewEdge.to = edge.from;
				NewEdge.from = edge.to;

				m_edges[edge.to].push_back(NewEdge);
			}
		}
	}
}

void Graph::removeNode(IdxType node)
{
	assert(node < m_nodesCount);

	//set this node's index to invalid_node_index
	m_nodes[node].index = invalid_node_index;

	//if the graph is not directed remove all edges leading to this node and then
	//clear the edges leading from the node
	if (!m_digraph)
	{
		//visit each neighbour and erase any edges leading to this node
		for (EdgeList::iterator curEdge = m_edges[node].begin();
			curEdge != m_edges[node].end();
			++curEdge)
		{
			for (EdgeList::iterator curE = m_edges[curEdge->to].begin();
				curE != m_edges[curEdge->to].end();
				++curE)
			{
				if (curE->to == node)
				{
					curE = m_edges[curEdge->to].erase(curE);

					break;
				}
			}
		}

		//finally, clear this node's edges
		m_edges[node].clear();
	}

	//if a digraph remove the edges the slow way
	else
	{
		removeInvalidEdges();
	}
}

void Graph::removeEdge(IdxType from, IdxType to)
{
	assert((from < m_nodesCount) && (to < m_nodesCount));

	EdgeList::iterator curEdge;

	if (!m_digraph)
	{
		for (curEdge = m_edges[to].begin();
			curEdge != m_edges[to].end();
			++curEdge)
		{
			if (curEdge->to == from) { curEdge = m_edges[to].erase(curEdge); break; }
		}
	}

	for (curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to) { curEdge = m_edges[from].erase(curEdge); break; }
	}
}

void Graph::setEdgeCost(IdxType from, IdxType to, double cost)
{
	//make sure the nodes given are valid
	assert((from < m_nodes.size()) && (to < m_nodes.size()));

	//visit each neighbour and erase any edges leading to this node
	for (EdgeList::iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to)
		{
			curEdge->cost = cost;
			break;
		}
	}
}

bool Graph::isNodePresent(IdxType nd) const
{
	if ((m_nodes[nd].index == invalid_node_index) || (nd >= m_nodesCount))
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Graph::isEdgePresent(IdxType from, IdxType to) const
{
	if (isNodePresent(from) && isNodePresent(from))
	{
		for (EdgeList::const_iterator curEdge = m_edges[from].begin();
			curEdge != m_edges[from].end();
			++curEdge)
		{
			if (curEdge->to == to) return true;
		}

		return false;
	}
	else
	{
		return false;
	}
}

bool Graph::isUniqueEdge(IdxType from, IdxType to)
{
	for (EdgeList::const_iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to)
		{
			return false;
		}
	}

	return true;
}

void Graph::removeInvalidEdges()
{
	for (EdgeListVector::iterator curEdgeList = m_edges.begin(); curEdgeList != m_edges.end(); ++curEdgeList)
	{
		for (EdgeList::iterator curEdge = (*curEdgeList).begin(); curEdge != (*curEdgeList).end(); ++curEdge)
		{
			if (m_nodes[curEdge->to].index == invalid_node_index ||
				m_nodes[curEdge->from].index == invalid_node_index)
			{
				curEdge = (*curEdgeList).erase(curEdge);
			}
		}
	}
}
